// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312_Mod1.h"
#include "MainCharacter.h"

//Initialize variables.
float maxWalkSpeed = 0.0f;
UCharacterMovementComponent* characterMovement;
APlayerController* KeyPlayerController;
UCameraComponent* actorCam;
FVector thirdPersonView;
FVector firstPersonView;

// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	//When the game starts, get the character's movement class and get the game default 'MaxWalkSpeed' of the character.
	characterMovement = GetCharacterMovement();
	maxWalkSpeed = characterMovement->MaxWalkSpeed;

	//When the game starts, get the player controller so that we can set which camera in the scene to use.
	KeyPlayerController = UGameplayStatics::GetPlayerController(this, 0);

	//Get the needed camera actors from the scene based on their name.
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr->GetName() == "FixedCamOne")
		{
			fixedCamOne = *ActorItr;
		}
		else if (ActorItr->GetName() == "FixedCamTwo") 
		{
			fixedCamTwo = *ActorItr;
		}
	}

	//Get the camera component from the character. This will be used to switch between first and third person views.
	actorCam = (UCameraComponent*)GetDefaultSubobjectByName(TEXT("ThirdPersonCam"));

	//Set the locations for the first/third person camera views.
	//On startup the camera is in the third person view position, so get that position and save it to 'thirdPersonView'.
	thirdPersonView = actorCam->RelativeLocation;
	firstPersonView = FVector(0, 0, 50.0f + BaseEyeHeight);
}

// Called every frame
void AMainCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(InputComponent);
	//Bind the "Lateral" and "SidetoSide" movements to this character and call their respective functions.
	InputComponent->BindAxis("Lateral", this, &AMainCharacter::Lateral);
	InputComponent->BindAxis("SidetoSide", this, &AMainCharacter::SidetoSide);

	//Bind the "Run" button to the the following functions:
	InputComponent->BindAction("Run", IE_Pressed, this, &AMainCharacter::Run);
	InputComponent->BindAction("Run", IE_Released, this, &AMainCharacter::StopRun);

	//Bind the "Camera Change" buttons to the following functions:
	InputComponent->BindAxis("FixedCamOne", this, &AMainCharacter::useFixedCamOne);
	InputComponent->BindAxis("FixedCamTwo", this, &AMainCharacter::useFixedCamTwo);
	InputComponent->BindAxis("ThirdPersonCam", this, &AMainCharacter::useThirdPersonCam);
	InputComponent->BindAxis("FirstPersonCam", this, &AMainCharacter::useFirstPersonCam);
	InputComponent->BindAxis("Turn", this, &AMainCharacter::Turn);
}

//Move the character forward or backward based on the value from which key is being pressed.
//'W' being positive (1.0) will move the character forward based on the vector direction the actor is facing.
//'S' being negative (-1.0) will move the character backward based on the vector direction the actor is facing.
//Note, if we were to do the oppositve (-GetActorForwardVector()), 'W' would need to be negative and 'S' would need to be postitive (the opposite of what they are now).
void AMainCharacter::Lateral(float value)
{
	if(Controller && value)
	{
		FVector forward = GetActorForwardVector();
		AddMovementInput(forward, value);
	}
}

//Move the character left or right based on the value from which key is being pressed.
//'A' being negative (-1.0) will move the character left based on the which vector is in the actor's right direction.
//'S' being negative (1.0) will mov ethe character right based on which vector is in the actor's right direction.
//Note, if we were to do the opposite (-GetActorRightVector()), 'A' would need to be positive and 'S' would need to be negative (the opposite of what they are now).
void AMainCharacter::SidetoSide(float value)
{
	if(Controller && value)
	{
		FVector right = GetActorRightVector();
		AddMovementInput(right, value);
	}
}

//Double the 'MaxWalkSpeed' of the character when the 'left shift' button is pressed.
void AMainCharacter::Run() 
{
	characterMovement->MaxWalkSpeed = maxWalkSpeed * 2;
}

//Return the 'MaxWalkSpeed' to its default value when the 'left shift' button is released.
void AMainCharacter::StopRun() 
{
	characterMovement->MaxWalkSpeed = maxWalkSpeed;
}

//Switch to the first fixed camera in the scene.
void AMainCharacter::useFixedCamOne(float value)
{
	if (Controller && value)
	{
		KeyPlayerController->SetViewTarget(fixedCamOne);
	}
}

//Switch to the second fixed camera in the scene.
void AMainCharacter::useFixedCamTwo(float value)
{
	if (Controller && value)
	{
		KeyPlayerController->SetViewTarget(fixedCamTwo);
	}
}

//Move the camera attached to this character class to a third person view and then switch to it.
void AMainCharacter::useThirdPersonCam(float value)
{
	if (Controller && value)
	{
		//Set the camera location for third person viewing.
		actorCam->RelativeLocation = thirdPersonView;
		//Update the camera's world position before switching.
		actorCam->UpdateComponentToWorld();
		KeyPlayerController->SetViewTarget(this);
	}
}

//Move the camera attached to this character class to a first person view and then switch to it.
void AMainCharacter::useFirstPersonCam(float value)
{
	if (Controller && value)
	{
		//Set the camera location for first person viewing.
		actorCam->RelativeLocation = firstPersonView;
		//Update the camera's world position before switching.
		actorCam->UpdateComponentToWorld();
		KeyPlayerController->SetViewTarget(this);
	}
}

//Turn the camera based on the axis retrieved from 'Mouse X'.
void AMainCharacter::Turn(float AxisValue)
{
	FRotator NewRotation = GetActorRotation();
	NewRotation.Yaw += AxisValue;
	SetActorRotation(NewRotation);
}