// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "AICharacter.generated.h"

UCLASS()
class GAM312_MOD1_API AAICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAICharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	//Setup the behavior tree for this character. Make it so this can be edited anywhere.
	UPROPERTY(EditAnywhere, Category = "Behavior")
	class UBehaviorTree* behaviorTree;
};
