// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "AIFollowerController.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_MOD1_API AAIFollowerController : public AAIController
{
	GENERATED_BODY()
	
public:

	// Sets default values for this controller's properties
	AAIFollowerController();
	
	//Override the "Possess" call from the "AIController" class with our own.
	virtual void Possess(APawn* pawn) override;

	//Create new methods for this class to call.
	void setPlayerToFollow(APawn* pawn);

protected:

	//Initialize new protected variables for this class.
	UBehaviorTreeComponent* behaviorComp;
	UBlackboardComponent* blackboardComp;
	
};
