// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312_Mod1.h"
#include "CollectibleObject.h"
#include "MainCharacter.h"


// Sets default values
ACollectibleObject::ACollectibleObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	//Create a pawn using a sphere root component that reacts to physics.
	USphereComponent* Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = Sphere;
	Sphere->InitSphereRadius(80.0f); //Set the size of the sphere.
	Sphere->SetCollisionProfileName(TEXT("OverlapAll"));
	Sphere->bGenerateOverlapEvents = true;

	//Create and position a mesh component so the sphere is visible.
	UStaticMeshComponent* SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	SphereMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));

	//If we can attach a mesh component to the sphere, position and show it in the world.
	if (SphereVisualAsset.Succeeded())
	{
		SphereMesh->SetStaticMesh(SphereVisualAsset.Object);
		SphereMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -60.0f));
		SphereMesh->SetWorldScale3D(FVector(1.2f));
	}

	//Setup a notification for when this component overlaps an object.
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &ACollectibleObject::OnOverlapBegin);
}

//This function is called when this component overlaps another object.
void ACollectibleObject::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//Check if the other actor is initiated and that the other object isn't this object.
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//Get world data and the player controlled character.
		UWorld* wRef = GetWorld();
		AMainCharacter* mChar = Cast<AMainCharacter>(wRef->GetFirstPlayerController()->GetCharacter());

		//If the overlapping object is the player, add one to player's collected objects count and destroy this object.
		if (mChar && mChar == OtherActor) 
		{
			mChar->collectedObjects += 1;
			Destroy();
		}
	}
}

// Called when the game starts or when spawned
void ACollectibleObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollectibleObject::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

