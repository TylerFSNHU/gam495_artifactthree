// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312_Mod1.h"
#include "AICharacter.h"
#include "AIFollowerController.h"
#include "MainCharacter.h"

// Sets default values
AAICharacter::AAICharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAICharacter::BeginPlay()
{
	Super::BeginPlay();
	
	//Set the controller for this AI character.
	AAIFollowerController* aiController = Cast<AAIFollowerController>(GetController());
	
	//Have the AI follow this character.
	AMainCharacter* playerCharacter = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));

	//Check if the aiController exists.
	if (aiController)
	{
		//If the aiController exists, set it to follow the player character.
		aiController->setPlayerToFollow(playerCharacter);
	}
}