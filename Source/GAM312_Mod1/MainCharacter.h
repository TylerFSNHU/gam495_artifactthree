// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UCLASS()
class GAM312_MOD1_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//These functions are used to move the character forward, backward, left, and right.
	void Lateral(float value); //Forward and Backward
	void SidetoSide(float value); //Right and Left

	//These functions are used to change the walk speed of the character.
	void Run(); //Move the character faster (sprint).
	void StopRun(); //Resume normal walk speed.

	//These functions are used to change which camera is currently being used.
	void useFixedCamOne(float value);
	void useFixedCamTwo(float value);
	void useThirdPersonCam(float value);
	void useFirstPersonCam(float value);

	//This function is used to turn the camera.
	void Turn(float AxisValue);

	//Keep track of how many objects the player has connected. Make this visible to blueprints for displaying on the player UI.
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int collectedObjects = 0;

private:
	//Initialize these actors that will be used to store the camera actors in scene.
	AActor* fixedCamOne;
	AActor* fixedCamTwo;
};
