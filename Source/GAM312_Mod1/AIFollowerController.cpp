// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312_Mod1.h"
#include "AIFollowerController.h"
#include "AICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"

// Sets default values
AAIFollowerController::AAIFollowerController()
{
	behaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(FName("behaviorComp"));
	blackboardComp = CreateDefaultSubobject<UBlackboardComponent>(FName("blackboardComp"));
}

//This class is called when this controller becomes possessed by a pawn.
void AAIFollowerController::Possess(APawn* pawn)
{
	//Call the original "Possess" class.
	Super::Possess(pawn);

	//Get the character class from the current pawn.
	AAICharacter* aiChar = Cast<AAICharacter>(pawn);

	//Check if the aiChar exists.
	if (aiChar) 
	{
		//If the aiChar exists, initialize the blackboard and behavior tree and start the tree.
		//The tree is used to constantly follow the player character.
		if (aiChar->behaviorTree->BlackboardAsset) 
		{
			blackboardComp->InitializeBlackboard(*(aiChar->behaviorTree->BlackboardAsset));
			behaviorComp->StartTree(*aiChar->behaviorTree);
		}
	}
}

//This class sets which player the AI Character should follow.
void AAIFollowerController::setPlayerToFollow(APawn* pawn)
{
	//Check if the blackboard component exists.
	if (blackboardComp) 
	{
		//If the blackboard exists, set "playerToFollow" as the pawn passed into this method.
		blackboardComp->SetValueAsObject(FName("playerToFollow"), pawn);
	}
}
